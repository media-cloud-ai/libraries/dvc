# DVC

This crate is a small object tracker for DVC repo that provides the following functionalities:
- Track a DVC repository managed with Git
- Download locally a specific resource (a dataset or a serialized model) in that repo
- Update the resource to its last version

## List of supported remote storage

- [x] S3 buckets
- [ ] GCS
- [ ] Google Drive
- [ ] Microsoft Azure

## Features

This crates comes with a Python feature that wraps DVC's Python API.

## Limitations

As of now, this crate presents some limitations:
- Only the remote default branch can be fetched (and a default branch **must** exist)