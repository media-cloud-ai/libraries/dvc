use crate::{error::TransferError, Error};
use async_std::channel;
use rs_transfer::{
  reader::*,
  secret::Secret,
  writer::{DummyWriteJob, FileWriter, StreamWriter},
  StreamData,
};
use std::sync::{Arc, Mutex};

pub async fn start_reader(
  source_path: &str,
  source_secret: Secret,
  sender: channel::Sender<StreamData>,
) -> Result<u64, Error> {
  let runtime = tokio::runtime::Runtime::new().unwrap();
  let runtime = Arc::new(Mutex::new(runtime));
  let simple_reader = SimpleReader {};
  match source_secret {
    Secret::S3 {
      hostname,
      access_key_id,
      secret_access_key,
      region,
      bucket,
    } => {
      let reader = S3Reader {
        hostname,
        access_key_id,
        secret_access_key,
        region,
        bucket,
        runtime,
      };
      reader
        .read_stream(source_path, sender, &simple_reader)
        .await
        .map_err(|e| Error::Transfer(TransferError::Read(e)))
    }

    _ => Err(Error::Transfer(TransferError::UnimplementedStorageType)),
  }
}

pub async fn start_writer(
  destination_path: &str,
  destination_secret: Secret,
  receiver: channel::Receiver<StreamData>,
) -> Result<(), Error> {
  let simple_writer = DummyWriteJob {};
  match destination_secret {
    Secret::Local => {
      let writer = FileWriter {};
      writer
        .write_stream(destination_path, receiver, &simple_writer)
        .await
        .map_err(|e| Error::Transfer(TransferError::Write(e)))
    }
    _ => Err(Error::Transfer(TransferError::UnimplementedStorageType)),
  }
}
