use crate::error::{self, Error, FileError, Result};
use serde::Deserialize;
use std::{fs, path::PathBuf};

#[derive(Debug, Deserialize)]
pub struct DvcYamlTrackingFile {
  outs: Vec<DvcDataItem>,
}

#[derive(Debug, Deserialize)]
pub struct DvcDataItem {
  #[serde(rename = "md5")]
  checksum: String,
  // size: u64,
  // path: String,
}

impl DvcYamlTrackingFile {
  pub fn new(file_path: PathBuf) -> Result<Self> {
    let data_file_content = fs::read_to_string(file_path)?;
    let parsed_data_file: DvcYamlTrackingFile = serde_yaml::from_str(&data_file_content)
      .map_err(|e| Error::File(error::FileError::ParseYaml(e)))?;
    Ok(parsed_data_file)
  }

  pub fn get_resource_path(&self) -> Result<String> {
    let object_info = self.outs.first().ok_or_else(|| {
      Error::File(FileError::Structure(
        "Can't find first element of outs array in Yaml file.".to_string(),
      ))
    })?;

    let checksum = &*object_info.checksum;

    let splitted_checksum = checksum.split_at(2);
    Ok(format!("{}/{}", splitted_checksum.0, splitted_checksum.1))
  }
}
