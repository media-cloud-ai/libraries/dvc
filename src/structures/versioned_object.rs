use crate::{
  download,
  error::{Error, RemoteError, Result, TransferError},
  structures::{DvcRemoteTarget, DvcRepo},
};
use async_std::{channel, task};
use rs_transfer::secret::Secret;
use std::path::Path;
use std::thread;
use url::Url;

#[derive(PartialEq, Debug)]
pub struct DvcVersionedObject {
  download_folder: Box<Path>,
  remote_secret: Secret,
  remote_target: DvcRemoteTarget,
  remote_path: String,
}

impl DvcVersionedObject {
  pub fn new(
    remote_target: DvcRemoteTarget,
    remote_secret: Secret,
    download_folder: Box<Path>,
  ) -> Self {
    let mut dvc_versioned_object = DvcVersionedObject {
      download_folder,
      remote_secret,
      remote_target,
      remote_path: String::new(),
    };
    let remote_path = dvc_versioned_object.build_remote_url().unwrap_or_default();
    dvc_versioned_object.remote_path = remote_path;
    dvc_versioned_object
  }

  pub fn has_same_dvc_repo(&self, repo: DvcRepo) -> bool {
    self.remote_target.repo == repo
  }

  pub fn get_local_absolute_filename(&self) -> String {
    let filename = self.download_folder.join(self.remote_path.clone());
    filename.to_str().unwrap().to_string()
  }

  fn build_remote_url(&mut self) -> Result<String> {
    let parsed_url = Url::parse(&*self.remote_target.object_url)
      .map_err(|_e| Error::Store(RemoteError::UnexpectedUrl))?;
    let source_secret = &mut self.remote_secret;

    if let Secret::S3 { bucket, .. } = source_secret {
      let storage_type = parsed_url.scheme();
      if storage_type != "s3" {
        return Err(Error::Store(RemoteError::InconsistentStorageTypes));
      }
      *bucket = parsed_url
        .host()
        .ok_or_else(|| {
          Error::Store(RemoteError::BucketError(
            "Unable to get bucket name".to_string(),
          ))
        })?
        .to_string();

      let path = parsed_url.path().trim_start_matches('/').to_string();
      Ok(path)
    } else {
      Err(Error::Store(RemoteError::UnimplementedStorage))
    }
  }

  pub fn download(&self) -> Result<()> {
    let (sender, receiver) = channel::unbounded();
    let cloned_source_secret = self.remote_secret.clone();
    let destination_path = self.get_local_absolute_filename();
    let cloned_object_path = self.remote_path.clone();

    let read_task = thread::spawn(move || {
      task::block_on(async {
        download::start_reader(&cloned_object_path, cloned_source_secret, sender).await
      })
    });
    let write_task = thread::spawn(move || {
      task::block_on(async {
        download::start_writer(&destination_path, Secret::Local, receiver).await
      })
    });

    read_task
      .join()
      .map_err(|_| Error::Transfer(TransferError::ThreadError))??;

    write_task
      .join()
      .map_err(|_| Error::Transfer(TransferError::ThreadError))??;

    Ok(())
  }

  pub fn latest(&mut self) -> Result<Option<Self>> {
    self.remote_target.update_object_url().map(|rt| {
      rt.map(|remote_target| {
        DvcVersionedObject::new(
          remote_target,
          self.remote_secret.clone(),
          self.download_folder.clone(),
        )
      })
    })
  }
}

#[test]
pub fn test_dvc_versioned_object_s3() {
  use std::{convert::TryFrom, path::PathBuf, process::Command};

  let mut update = Command::new("tests/scripts/setup.sh")
    .arg("/tmp/test_dvc_versioned_object_s3")
    .spawn()
    .unwrap();
  let _result = update.wait().unwrap();

  let dvc_repo = DvcRepo::new("data/data.txt", "/tmp/test_dvc_versioned_object_s3");
  let dvc_remote = DvcRemoteTarget::try_from(dvc_repo).unwrap();

  let s3_hostname =
    std::env::var("S3_HOSTNAME").unwrap_or_else(|_| "http://localhost:9000".to_string());
  let s3_username = std::env::var("MINIO_ACCESS_KEY").unwrap_or_else(|_| "minioadmin".to_string());
  let s3_password = std::env::var("MINIO_SECRET_KEY").unwrap_or_else(|_| "minioadmin".to_string());

  let s3_secret = Secret::S3 {
    hostname: Some(s3_hostname.clone()),
    access_key_id: s3_username.clone(),
    secret_access_key: s3_password.clone(),
    region: None,
    bucket: String::new(),
  };

  let dvc_versioned_object = DvcVersionedObject::new(
    dvc_remote.clone(),
    s3_secret,
    PathBuf::from("/tmp").into_boxed_path(),
  );

  let expected_s3_secret = Secret::S3 {
    hostname: Some(s3_hostname),
    access_key_id: s3_username,
    secret_access_key: s3_password,
    region: None,
    bucket: "dvc".to_string(),
  };
  assert_eq!(
    dvc_versioned_object,
    DvcVersionedObject {
      download_folder: PathBuf::from("/tmp").into_boxed_path(),
      remote_secret: expected_s3_secret,
      remote_target: dvc_remote,
      remote_path: "d5/53b24705175b508f742e677a5b7b19".to_string()
    }
  )
}

#[test]
pub fn test_download_s3() {
  use std::{convert::TryFrom, fs, path::PathBuf, process::Command};

  let mut update = Command::new("tests/scripts/setup.sh")
    .arg("/tmp/test_download_s3")
    .spawn()
    .unwrap();
  let _result = update.wait().unwrap();

  let mut push = Command::new("tests/scripts/push.sh")
    .arg("/tmp/test_download_s3")
    .spawn()
    .unwrap();
  let _result = push.wait().unwrap();

  let dvc_repo = DvcRepo::new("data/data.txt", "/tmp/test_download_s3");
  let dvc_remote = DvcRemoteTarget::try_from(dvc_repo).unwrap();

  let s3_hostname =
    std::env::var("S3_HOSTNAME").unwrap_or_else(|_| "http://localhost:9000".to_string());
  let s3_username = std::env::var("MINIO_ACCESS_KEY").unwrap_or_else(|_| "minioadmin".to_string());
  let s3_password = std::env::var("MINIO_SECRET_KEY").unwrap_or_else(|_| "minioadmin".to_string());

  let s3_secret = Secret::S3 {
    hostname: Some(s3_hostname),
    access_key_id: s3_username,
    secret_access_key: s3_password,
    region: None,
    bucket: String::new(),
  };

  let dvc_versioned_object = DvcVersionedObject::new(
    dvc_remote,
    s3_secret,
    PathBuf::from("/tmp/test_download_s3/download").into_boxed_path(),
  );

  dvc_versioned_object.download().unwrap();
  let download_content = fs::read_to_string(format!(
    "{}/{}",
    "/tmp/test_download_s3/download", dvc_versioned_object.remote_path
  ))
  .unwrap();
  assert_eq!(download_content, "Some data\n")
}

#[test]
pub fn test_latest_s3() {
  use std::{convert::TryFrom, path::PathBuf, process::Command};

  let mut update = Command::new("tests/scripts/setup.sh")
    .arg("/tmp/test_latest_s3")
    .spawn()
    .unwrap();
  let _result = update.wait().unwrap();

  let dvc_repo = DvcRepo::new("data/data.txt", "/tmp/test_latest_s3");
  let dvc_remote = DvcRemoteTarget::try_from(dvc_repo).unwrap();

  let s3_hostname =
    std::env::var("S3_HOSTNAME").unwrap_or_else(|_| "http://localhost:9000".to_string());
  let s3_username = std::env::var("MINIO_ACCESS_KEY").unwrap_or_else(|_| "minioadmin".to_string());
  let s3_password = std::env::var("MINIO_SECRET_KEY").unwrap_or_else(|_| "minioadmin".to_string());

  let s3_secret = Secret::S3 {
    hostname: Some(s3_hostname.clone()),
    access_key_id: s3_username.clone(),
    secret_access_key: s3_password.clone(),
    region: None,
    bucket: String::new(),
  };

  let mut dvc_versioned_object = DvcVersionedObject::new(
    dvc_remote.clone(),
    s3_secret,
    PathBuf::from("/tmp").into_boxed_path(),
  );

  let mut update = Command::new("tests/scripts/update.sh")
    .arg("/tmp/test_latest_s3")
    .spawn()
    .unwrap();
  let _result = update.wait().unwrap();

  let updated_dvc_versioned_object = dvc_versioned_object.latest().unwrap();

  let expected_s3_secret = Secret::S3 {
    hostname: Some(s3_hostname),
    access_key_id: s3_username,
    secret_access_key: s3_password,
    region: None,
    bucket: "dvc".to_string(),
  };

  let mut expected_dvc_remote = dvc_remote;
  expected_dvc_remote.object_url = "s3://dvc/5f/4492a041cc7e11059cef5a504e2c6b".to_string();
  println!("{:?}", updated_dvc_versioned_object);

  assert_eq!(
    updated_dvc_versioned_object,
    Some(DvcVersionedObject {
      download_folder: PathBuf::from("/tmp").into_boxed_path(),
      remote_secret: expected_s3_secret,
      remote_target: expected_dvc_remote,
      remote_path: "5f/4492a041cc7e11059cef5a504e2c6b".to_string()
    })
  )
}
