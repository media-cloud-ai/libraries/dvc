#[cfg(not(feature = "python_dvc"))]
mod dvc_ini_config;
mod dvc_repo;
#[cfg(not(feature = "python_dvc"))]
mod dvc_yaml_tracking_file;
mod remote_target;
mod versioned_object;

#[cfg(not(feature = "python_dvc"))]
pub use dvc_ini_config::DvcIniConfig;
pub use dvc_repo::DvcRepo;
#[cfg(not(feature = "python_dvc"))]
pub use dvc_yaml_tracking_file::DvcYamlTrackingFile;
pub use remote_target::DvcRemoteTarget;
pub use versioned_object::DvcVersionedObject;

#[derive(Clone, Debug, PartialEq)]
pub struct GitToken {
  pub name: String,
  pub value: String,
}
