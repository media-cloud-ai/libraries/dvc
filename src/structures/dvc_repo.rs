use crate::error::{Error, Result};
use crate::structures::GitToken;

#[cfg(not(feature = "python_dvc"))]
use std::path::PathBuf;

#[derive(Clone, Debug, PartialEq)]
pub struct DvcRepo {
  #[cfg(not(feature = "python_dvc"))]
  clone_path: PathBuf,
  path: String,
  git_repo: String,
  rev: Option<String>,
  remote: Option<String>,
  token: Option<GitToken>,
}

impl DvcRepo {
  pub fn with_rev(mut self, rev: &str) -> Self {
    self.rev = Some(rev.to_string());
    self
  }

  pub fn with_remote(mut self, remote: &str) -> Self {
    self.remote = Some(remote.to_string());
    self
  }

  pub fn with_token(mut self, token: GitToken) -> Self {
    self.token = Some(token);
    self
  }
}

#[cfg(feature = "python_dvc")]
impl DvcRepo {
  pub fn new(path: &str, repo: &str) -> Self {
    DvcRepo {
      path: path.to_string(),
      git_repo: repo.to_string(),
      rev: None,
      remote: None,
      token: None,
    }
  }

  pub(crate) fn resolve_object_url(&self) -> Result<String> {
    use pyo3::{prelude::*, types::IntoPyDict};

    Python::with_gil(|py| {
      let dvc_api = PyModule::import(py, "dvc.api")?;
      let git_repo_url = self.resolve_repo_authenticated_url()?;
      let kwargs = [
        ("path", self.path.clone()),
        ("repo", git_repo_url),
        ("rev", self.rev.clone().unwrap_or_default()),
        ("remote", self.remote.clone().unwrap_or_default()),
      ]
      .into_py_dict(py);
      let object_url: String = dvc_api
        .getattr("get_url")?
        .call((), Some(kwargs))?
        .extract()?;

      Ok(object_url)
    })
  }

  fn resolve_repo_authenticated_url(&self) -> Result<String> {
    use url::Url;
    let cloned_token = self.token.clone();
    if let Some(token) = cloned_token {
      let mut parsed_repo_url = Url::parse(&self.git_repo)
        .map_err(|_| Error::Git("Unable to parse repo URL".to_string()))?;
      parsed_repo_url
        .set_username(&token.name)
        .map_err(|_| Error::Git("Unable to set username".to_string()))?;
      parsed_repo_url
        .set_password(Some(&token.value))
        .map_err(|_| Error::Git("Unable to set password".to_string()))?;
      return Ok(parsed_repo_url.to_string());
    }
    Ok(self.git_repo.clone())
  }
}

#[cfg(not(feature = "python_dvc"))]
impl DvcRepo {
  pub fn new(path: &str, repo: &str) -> Self {
    use std::env;
    use uuid::Uuid;
    let clone_path = env::temp_dir().join(Uuid::new_v4().to_string());

    DvcRepo {
      clone_path,
      path: path.to_string(),
      git_repo: repo.to_string(),
      rev: None,
      remote: None,
      token: None,
    }
  }

  pub(crate) fn resolve_object_url(&self) -> Result<String> {
    self.clone_repository()?;
    let base = self.get_base_url()?;
    let resource = self.get_resource_path()?;
    Ok(format!("{}/{}", base, resource))
  }

  fn clone_repository(&self) -> Result<()> {
    if self.clone_path.read_dir().is_err() {
      let cloned_token = self.token.clone();
      let mut callbacks = git2::RemoteCallbacks::new();

      if let Some(token) = cloned_token {
        callbacks.credentials(move |_url, _username_from_url, _allowed_types| {
          git2::Cred::userpass_plaintext(&token.name, &token.value)
        });
      }

      let mut fetch_option = git2::FetchOptions::new();
      fetch_option.remote_callbacks(callbacks);

      let mut builder = git2::build::RepoBuilder::new();
      builder.fetch_options(fetch_option);

      builder
        .clone(&self.git_repo, &self.clone_path)
        .map_err(|e| Error::Git(e.to_string()))?;
    } else {
      self.pull_repository()?;
    }
    Ok(())
  }

  fn pull_repository(&self) -> Result<()> {
    let repo = git2::Repository::open(&self.clone_path)?;
    let mut remote = repo.find_remote("origin")?;

    let cloned_token = self.token.clone();
    let mut callbacks = git2::RemoteCallbacks::new();
    if let Some(token) = cloned_token {
      callbacks.credentials(move |_url, _username_from_url, _allowed_types| {
        git2::Cred::userpass_plaintext(&token.name, &token.value)
      });
    }
    let mut remote_connection =
      remote.connect_auth(git2::Direction::Fetch, Some(callbacks), None)?;
    let branch = remote_connection.default_branch()?;
    let branch = branch
      .as_str()
      .ok_or_else(|| Error::Git("Unable to get remote's default branch.".to_string()))?;
    println!("{:?}", branch);

    remote_connection.remote().fetch(&[branch], None, None)?;

    let fetch_head = repo.find_reference("FETCH_HEAD")?;
    let fetch_commit = repo.reference_to_annotated_commit(&fetch_head)?;
    let analysis = repo.merge_analysis(&[&fetch_commit])?;
    if analysis.0.is_up_to_date() {
      Ok(())
    } else if analysis.0.is_fast_forward() {
      let mut reference = repo.find_reference(branch)?;
      reference.set_target(fetch_commit.id(), "Fast-Forward")?;
      repo.set_head(branch)?;
      repo.checkout_head(Some(git2::build::CheckoutBuilder::new().force()))?;
      Ok(())
    } else {
      Err(Error::Git("Fast-forward only.".to_string()))
    }
  }

  fn get_base_url(&self) -> Result<String> {
    use crate::structures::DvcIniConfig;

    let dvc_config = DvcIniConfig::new(self.clone_path.clone())?;
    dvc_config.get_remote_url()
  }

  fn get_resource_path(&self) -> Result<String> {
    use crate::structures::DvcYamlTrackingFile;
    use std::path::Path;

    let mut dvc_extension = Path::new(&self.path)
      .extension()
      .unwrap_or_default()
      .to_str()
      .unwrap_or_default()
      .to_string();
    dvc_extension.push_str(".dvc");

    let file_path = self
      .clone_path
      .join(self.path.clone())
      .with_extension(dvc_extension);

    let yaml_tracking_file = DvcYamlTrackingFile::new(file_path)?;
    yaml_tracking_file.get_resource_path()
  }
}

#[test]
#[cfg(feature = "python_dvc")]
pub fn test_dvc_repo_init() {
  let dvc_repo = DvcRepo::new("/test", "/tests/dvc")
    .with_rev("FFFFFFFF")
    .with_remote("develop")
    .with_token(GitToken {
      name: "dvc".to_string(),
      value: "token".to_string(),
    });

  assert_eq!(
    dvc_repo,
    DvcRepo {
      path: "/test".to_string(),
      git_repo: "/tests/dvc".to_string(),
      rev: Some("FFFFFFFF".to_string()),
      remote: Some("develop".to_string()),
      token: Some(GitToken {
        name: "dvc".to_string(),
        value: "token".to_string(),
      })
    }
  )
}

#[test]
#[cfg(not(feature = "python_dvc"))]
pub fn test_dvc_repo_init() {
  let dvc_repo = DvcRepo::new("/test", "/tests/dvc")
    .with_rev("FFFFFFFF")
    .with_remote("develop")
    .with_token(GitToken {
      name: "dvc".to_string(),
      value: "token".to_string(),
    });

  let clone_path = dvc_repo.clone_path.clone();

  assert_eq!(
    dvc_repo,
    DvcRepo {
      clone_path,
      path: "/test".to_string(),
      git_repo: "/tests/dvc".to_string(),
      rev: Some("FFFFFFFF".to_string()),
      remote: Some("develop".to_string()),
      token: Some(GitToken {
        name: "dvc".to_string(),
        value: "token".to_string(),
      })
    }
  )
}
