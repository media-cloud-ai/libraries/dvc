use crate::error::{Error, FileError, Result};
use ini::Ini;
use std::path::PathBuf;

#[derive(Clone)]
pub struct DvcIniConfig {
  config: Ini,
}

impl DvcIniConfig {
  pub fn new(repo_path: PathBuf) -> Result<Self> {
    let config_path = repo_path.join(".dvc/config");
    let config = Ini::load_from_file(config_path)?;

    Ok(DvcIniConfig { config })
  }

  // We assume that a default remote is specified
  pub fn get_remote_name(&self) -> Result<String> {
    let storage_name = self.config.section(Some("core")).ok_or_else(|| {
      Error::File(FileError::Structure(
        "Could not resolve core section in dvc config file.".to_string(),
      ))
    })?;

    storage_name
      .get("remote".to_string())
      .ok_or_else(|| {
        Error::File(FileError::Structure(
          "No default storage configured.".to_string(),
        ))
      })
      .map(|s| s.to_string())
  }

  pub fn get_remote_url(&self) -> Result<String> {
    let remote_section = self
      .config
      .section(Some(format!(r#"'remote "{}"'"#, self.get_remote_name()?)))
      .ok_or_else(|| {
        Error::File(FileError::Structure(
          "Unable resolve storage section in dvc config file".to_string(),
        ))
      })?;

    remote_section
      .get("url")
      .ok_or_else(|| {
        Error::File(FileError::Structure(
          "Unable to extract remote storage url.".to_string(),
        ))
      })
      .map(|s| s.to_string())
  }
}
