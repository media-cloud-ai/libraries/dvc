use crate::error::{Error, Result};
use crate::structures::DvcRepo;
use std::convert::TryFrom;

#[derive(Clone, Debug, PartialEq)]
pub struct DvcRemoteTarget {
  pub repo: DvcRepo,
  pub object_url: String,
}

impl TryFrom<DvcRepo> for DvcRemoteTarget {
  type Error = Error;

  fn try_from(dvc_repo: DvcRepo) -> std::result::Result<Self, Self::Error> {
    let object_url = dvc_repo.resolve_object_url()?;
    Ok(DvcRemoteTarget {
      repo: dvc_repo,
      object_url,
    })
  }
}

impl DvcRemoteTarget {
  pub fn update_object_url(&mut self) -> Result<Option<DvcRemoteTarget>> {
    let new_object_url = self.repo.resolve_object_url()?;
    Ok(
      (self.object_url != new_object_url).then(|| DvcRemoteTarget {
        repo: self.repo.clone(),
        object_url: new_object_url,
      }),
    )
  }
}

#[test]
pub fn test_dvc_remote_target() {
  use std::process::Command;

  let mut update = Command::new("tests/scripts/setup.sh")
    .arg("/tmp/test_dvc_remote_target")
    .spawn()
    .unwrap();
  let _result = update.wait().unwrap();
  let dvc_repo = DvcRepo::new("data/data.txt", "/tmp/test_dvc_remote_target");
  let dvc_remote = DvcRemoteTarget::try_from(dvc_repo.clone()).unwrap();

  assert_eq!(
    dvc_remote,
    DvcRemoteTarget {
      repo: dvc_repo,
      object_url: "s3://dvc/d5/53b24705175b508f742e677a5b7b19".to_string(),
    }
  )
}
