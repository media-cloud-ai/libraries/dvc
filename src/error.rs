use crate::error::FileError::ParseIni;
#[cfg(feature = "python_dvc")]
use pyo3::PyErr;
use std::io;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum TransferError {
  Read(std::io::Error),
  Write(std::io::Error),
  UnimplementedStorageType,
  ThreadError,
}

#[derive(Debug)]
pub enum RemoteError {
  InconsistentStorageTypes,
  UnimplementedStorage,
  UnexpectedUrl,
  BucketError(String),
}

#[derive(Debug)]
pub enum FileError {
  IO(std::io::Error),
  ParseIni(ini::Error),
  ParseYaml(serde_yaml::Error),
  Structure(String),
}

#[derive(Debug)]
pub enum Error {
  #[cfg(feature = "python_dvc")]
  Dvc(PyErr),
  File(FileError),
  Git(String),
  Store(RemoteError),
  Transfer(TransferError),
}

#[cfg(feature = "python_dvc")]
impl From<PyErr> for Error {
  fn from(py_err: PyErr) -> Self {
    Error::Dvc(py_err)
  }
}

impl From<std::io::Error> for Error {
  fn from(io_err: io::Error) -> Self {
    Error::File(FileError::IO(io_err))
  }
}

impl From<git2::Error> for Error {
  fn from(git_err: git2::Error) -> Self {
    Error::Git(git_err.to_string())
  }
}

impl From<ini::Error> for Error {
  fn from(ini_error: ini::Error) -> Self {
    Error::File(ParseIni(ini_error))
  }
}
