#!/bin/bash

cd $1
dvc remote modify storage endpointurl "$S3_HOSTNAME"
dvc remote modify storage secret_access_key "$MINIO_ACCESS_KEY"
dvc remote modify storage access_key_id "$MINIO_SECRET_KEY"
dvc push