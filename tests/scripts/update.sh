#!/bin/bash

cd $1
echo "Some modified data" > data/data.txt
dvc add data/data.txt
git add data/data.txt.dvc
git commit -m "Modify dataset"