#!/bin/bash

rm -rf $1
mkdir $1
cd $1
git init
git config user.email ""
git config user.name "CI"
git config init.defaultBranch master
dvc init
git commit -m "Initialize DVC"
mkdir data
echo "Some data" > data/data.txt
dvc add data/data.txt
git add data/data.txt.dvc data/.gitignore
git commit -m "Add data"
dvc remote add storage s3://dvc
dvc remote default storage
git add .dvc/config
git commit -m "Add remote storage"